# Alphabet Analyzer
`AlphabetAnalyzer#determineOrdering()` is a function that determines the alphabet ordering for a sorted list of
words.

## Testing
This is a Maven project. To run the unit tests:
```bash
mvn test
```

## Components
`com.aaronwells.AlphabetAnalyzer` exposes a single function `determineOrdering()`, which infers the alphabet ordering
from a supplied `Iterator<String>`.

`com.aaronwells.GenericTopoSortGraph` provides a graph data structure that grows as new vertices are introduced, so it
is not required that the number of vertices be known ahead of time. It is used as a helper to the `determineOrdering()`
function and allows for a single pass through the word list. It is generic and assumes that basic equality and hash code 
implementations have been provided in the element type (which is the case for `java.lang.Character`).
`GenericTopoSortGraph` also exposes a `topologicalSort()` method to supply a topological sorting of all vertex elements 
in the graph.