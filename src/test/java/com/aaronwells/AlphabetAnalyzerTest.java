package com.aaronwells;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class AlphabetAnalyzerTest {
    // set up English alphabet list
    public static final int NUM_ENGLISH_ALPHA_CHARS = 26;
    private List<Character> englishAlphabet = Collections.unmodifiableList( Stream
            .iterate('a', c -> (char)(c + 1) )
            .limit(NUM_ENGLISH_ALPHA_CHARS)
            .collect(Collectors.toList()) );

    public static final String ENGLISH_DICTIONARY_FILE = "enable1.txt";

    @Test
    public void testEnglishAlphabet() {
        try (
                BufferedReader in = new BufferedReader(
                    new InputStreamReader( // Use Words With Friends 'enable1.txt' dictionary as input
                        getClass().getClassLoader().getResourceAsStream(ENGLISH_DICTIONARY_FILE)
                    )
                )
        ) {
            List<Character> actual = AlphabetAnalyzer.determineOrdering(
                    in.lines().iterator()
            );

            assertEquals(englishAlphabet, actual);
        } catch (IOException e) {
            fail("Could not open " + ENGLISH_DICTIONARY_FILE);
        }
    }

    @Test
    public void testExampleCase() {
        List<String> input = Arrays.asList("bca", "aaa", "acb");

        List<Character> expected = Arrays.asList('b', 'a', 'c');
        List<Character> actual = AlphabetAnalyzer.determineOrdering(input.iterator());

        assertEquals(expected, actual);
    }

    @Test
    public void testAlphabetWithDiacritics() {
        List<String> input = Arrays.asList("ébb", "ébd", "zéd", "zbé", "zbb");

        List<Character> expected = Arrays.asList('é', 'z', 'b', 'd');
        List<Character> actual = AlphabetAnalyzer.determineOrdering(input.iterator());

        assertEquals(expected, actual);
    }

    @Test
    public void testEmptyDictionaryInput() {
        List<String> input = Collections.emptyList();

        List<Character> actual = AlphabetAnalyzer.determineOrdering(input.iterator());

        assertTrue(actual.size() == 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOneWordDictionary() {
        List<String> input = Arrays.asList("word");

        AlphabetAnalyzer.determineOrdering(input.iterator());
    }

    @Test
    public void testEveryWordStartsWithSameLetter() {
        List<String> input = Arrays.asList("wz", "wy", "wx", "ww");

        List<Character> expected = Arrays.asList('z', 'y', 'x', 'w');
        List<Character> actual = AlphabetAnalyzer.determineOrdering(input.iterator());

        assertEquals(expected, actual);
    }

    @Test
    public void testOneCharWords() {
        List<String> input = Arrays.asList("d", "c", "a", "b");

        List<Character> expected = Arrays.asList('d', 'c', 'a', 'b');
        List<Character> actual = AlphabetAnalyzer.determineOrdering(input.iterator());

        assertEquals(expected, actual);
    }

    @Test
    public void testRepeatedWord() {
        List<String> input = Arrays.asList("yaz", "yaw", "yaw", "yay", "aaa");

        List<Character> expected = Arrays.asList('z', 'w', 'y', 'a');
        List<Character> actual = AlphabetAnalyzer.determineOrdering(input.iterator());

        assertEquals(expected, actual);
    }

}
