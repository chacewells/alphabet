package com.aaronwells;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class GenericTopoSortGraphTest {

    @Test
    public void testTopologicalSortZeroElements() {
        GenericTopoSortGraph g = new GenericTopoSortGraph<Integer>();
        int expectedSize = 0;

        List<Integer> output = g.topologicalSort();
        assertEquals("Returns an empty list", output.size(), expectedSize);
    }

    @Test
    public void testTopologicalSortDagTwoVerticesOneEdge() {
        GenericTopoSortGraph g = new GenericTopoSortGraph<Integer>();
        Integer a = 5;
        Integer b = 7;

        g.addEdge(a, b);
        List<Integer> expected = Arrays.asList(a, b);
        List<Integer> actual = g.topologicalSort();
        assertEquals("Lists are the same", expected, actual);
    }

    @Test
    public void testTopologicalSortDagThreeVerticesTwoEdges() {
        GenericTopoSortGraph g = new GenericTopoSortGraph<Integer>();
        Integer a = 5;
        Integer b = 7;
        Integer c = 1;

        g.addEdge(a, b);
        g.addEdge(b, c);
        List<Integer> expected = Arrays.asList(a, b, c);
        List<Integer> actual = g.topologicalSort();
        assertEquals("Lists are the same", expected, actual);
    }

    @Test
    public void testTopologicalSortDagThreeVerticesThreeEdges_firstEdgeMultipleEndpoints() {
        GenericTopoSortGraph g = new GenericTopoSortGraph<Integer>();
        Integer a = 5;
        Integer b = 7;
        Integer c = 1;

        g.addEdge(a, b);
        g.addEdge(b, c);
        g.addEdge(a, c);
        List<Integer> expected = Arrays.asList(a, b, c);
        List<Integer> actual = g.topologicalSort();
        assertEquals("Lists are the same", expected, actual);
    }

    @Test
    public void testTopologicalSortDagFourVerticesOneSourceOneDestination() {
        GenericTopoSortGraph g = new GenericTopoSortGraph<Integer>();
        Integer a = 3;
        Integer b = 2;
        Integer c = 5;
        Integer d = 1;

        g.addEdge(a, b);
        g.addEdge(a, c);
        g.addEdge(b, d);
        g.addEdge(c, d);

        List<Integer> actual = g.topologicalSort();
        int indexOfA = actual.indexOf(a);
        int indexOfB = actual.indexOf(b);
        int indexOfC = actual.indexOf(c);
        int indexOfD = actual.indexOf(d);
        assertTrue("a before b", indexOfA < indexOfB);
        assertTrue("a before c", indexOfA < indexOfC);
        assertTrue("b before d", indexOfB < indexOfD);
        assertTrue("c before d", indexOfC < indexOfD);
    }

}
