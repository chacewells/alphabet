package com.aaronwells;

import java.util.*;

public class GenericTopoSortGraph<E> {
    private ArrayList<LinkedList<Integer>> adjacencyList = new ArrayList<LinkedList<Integer>>();

    private Map<E, Integer> elementToIndexMapping = new HashMap<E, Integer>();
    private ArrayList<E> indexToElementMapping = new ArrayList<E>();

    public void addEdge(E v, E w) {
        Integer vIndex = getIndex(v);
        Integer wIndex = getIndex(w);

        LinkedList<Integer> endpoints = adjacencyList.get(vIndex);
        endpoints.add(wIndex);
    }

    private Integer getIndex(E e) {
        Integer index = elementToIndexMapping.get(e);
        if (index == null) { // no mapping; initialize
            indexToElementMapping.add(e);
            adjacencyList.add(new LinkedList<Integer>());
            index = indexToElementMapping.size() - 1;
            elementToIndexMapping.put(e, index);
        }
        return index;
    }

    public List<E> topologicalSort() {
        Stack<Integer> stack = new Stack<Integer>(); // Stores vertices _after_ visiting their adjacencies
        boolean[] visited = new boolean[adjacencyList.size()]; // Keep track of visits

        for (int i = 0; i < adjacencyList.size(); ++i) {
            if (!visited[i])
                this.topologicalSortUtil(i, visited, stack);
        }

        // Map stack vertices to their respective elements
        List<E> results = new LinkedList<E>();
        while (!stack.empty()) {
            Integer index = stack.pop();
            E element = indexToElementMapping.get(index);
            results.add( element );
        }
        return results;
    }

    private void topologicalSortUtil(int v, boolean[] visited, Stack<Integer> stack) {
        visited[v] = true;
        LinkedList<Integer> endpoints = adjacencyList.get(v);

        for (Integer i : endpoints) { // visit adjacencies for this vertex
            if (!visited[i])
                topologicalSortUtil(i, visited, stack);
        }

        stack.push(v);
    }

}
