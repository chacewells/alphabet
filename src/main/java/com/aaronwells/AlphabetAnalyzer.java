package com.aaronwells;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class AlphabetAnalyzer {

    public static List<Character> determineOrdering(Iterator<String> orderedWords) {
        GenericTopoSortGraph<Character> characterGraph = new GenericTopoSortGraph<>();
        String last;

        // No words, no alphabet
        if (!orderedWords.hasNext())
            return Collections.emptyList();

        last = orderedWords.next();

        // No comparisons, can't generate an ordering
        if (!orderedWords.hasNext())
            throw insufficientInformation();

        while (orderedWords.hasNext()) {
            String next = orderedWords.next();
            int minLength = Math.min(last.length(), next.length());
            for (int i = 0; i < minLength; ++i) {
                if (last.charAt(i) != next.charAt(i)) {
                    characterGraph.addEdge(last.charAt(i), next.charAt(i));
                    break;
                }
            }

            last = next;
        }

        return characterGraph.topologicalSort();
    }

    private static IllegalArgumentException insufficientInformation() {
        return new IllegalArgumentException("Insufficient information to determine ordering");
    }

}
